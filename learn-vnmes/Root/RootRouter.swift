//
//  RootRouter.swift
//  learn-vnmes
//
//  Created by Pham Anh Tuan on 11/16/19.
//  Copyright © 2019 Pham Anh Tuan. All rights reserved.
//

import RIBs

protocol RootInteractable: Interactable, LoginListener {
    var router: RootRouting? { get set }
    var listener: RootListener? { get set }
}

protocol RootViewControllable: ViewControllable {
    func present(viewController: ViewControllable)
}

final class RootRouter: LaunchRouter<RootInteractable, RootViewControllable>, RootRouting {

    init(interactor: RootInteractable,
         viewController: RootViewControllable, loginBuilder: LoginBuildable) {
        self.loginBuilder = loginBuilder
        super.init(interactor: interactor, viewController: viewController)
        interactor.router = self
    }

    override func didLoad() {
        super.didLoad()

        let login = loginBuilder.build(withListener: interactor)
        self.login = login
        attachChild(login)
        viewController.present(viewController: login.viewControllable)
    }

    public var loginBuilder: LoginBuildable
    private var login: ViewableRouting?
}
