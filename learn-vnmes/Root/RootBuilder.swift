//
//  RootBuilder.swift
//  learn-vnmes
//
//  Created by Pham Anh Tuan on 11/16/19.
//  Copyright © 2019 Pham Anh Tuan. All rights reserved.
//

import RIBs

protocol RootDependency: Dependency {
}

protocol RootDependencyLog : Dependency {
}



final class RootComponent: Component<RootDependency> {

    let window: UIWindow
    
    let rootViewController: RootViewController

    init(dependency: RootDependency,
         window: UIWindow,
         rootViewController: RootViewController) {
        self.rootViewController = rootViewController
        self.window = window
        super.init(dependency: dependency)
    }
}


// MARK: - Builder
protocol RootBuildable: Buildable {
    func build(with window: UIWindow) -> LaunchRouting
}

final class RootBuilder: Builder<RootDependency>, RootBuildable {

    override init(dependency: RootDependency) {
        super.init(dependency: dependency)
    }

    func build(with window: UIWindow) -> LaunchRouting {
       
        let viewController = RootViewController()
         let component = RootComponent(dependency: dependency, window: window, rootViewController: viewController)
        let interactor = RootInteractor(presenter: viewController)
        let loginBuilder = LoginBuilder(dependency: component)

        return RootRouter(interactor: interactor,
                          viewController: viewController,
                          loginBuilder: loginBuilder)
    }
}
