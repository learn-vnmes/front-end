//
//  RootInteractor.swift
//  learn-vnmes
//
//  Created by Pham Anh Tuan on 11/16/19.
//  Copyright © 2019 Pham Anh Tuan. All rights reserved.
//

import RIBs
import RxSwift

protocol RootRouting: ViewableRouting {
}

protocol RootPresentable: Presentable {
    var listener: RootPresentableListener? { get set }
}

protocol RootListener: class {
}

final class RootInteractor: PresentableInteractor<RootPresentable>, RootInteractable, RootPresentableListener {
    func detachLogin() {
        
    }
    

    weak var router: RootRouting?
    weak var listener: RootListener?

    override init(presenter: RootPresentable) {
        super.init(presenter: presenter)
        presenter.listener = self
    }

    override func didBecomeActive() {
        super.didBecomeActive()
    }

    override func willResignActive() {
        super.willResignActive()
    }
}
