// swiftlint:disable all
// Generated using SwiftGen — https://github.com/SwiftGen/SwiftGen

import Foundation

// swiftlint:disable superfluous_disable_command
// swiftlint:disable file_length

// MARK: - Strings

// swiftlint:disable explicit_type_interface function_parameter_count identifier_name line_length
// swiftlint:disable nesting type_body_length type_name
internal enum L10n {
  /// Đăng nhập
  internal static let loginButton = L10n.tr("Login", "Login_Button")
  /// Nhập tên đăng nhập và mật khẩu để vào trò chơi
  internal static let loginPlaceholderContent = L10n.tr("Login", "Login_Placeholder_Content")
  /// Đăng kí
  internal static let registerButton = L10n.tr("Login", "Register_Button")
  /// Bé học Tiếng Việt
  internal static let title = L10n.tr("Login", "Title")
}
// swiftlint:enable explicit_type_interface function_parameter_count identifier_name line_length
// swiftlint:enable nesting type_body_length type_name

// MARK: - Implementation Details

extension L10n {
  private static func tr(_ table: String, _ key: String, _ args: CVarArg...) -> String {
    // swiftlint:disable:next nslocalizedstring_key
    let format = NSLocalizedString(key, tableName: table, bundle: Bundle(for: BundleToken.self), comment: "")
    return String(format: format, locale: Locale.current, arguments: args)
  }
}

private final class BundleToken {}
