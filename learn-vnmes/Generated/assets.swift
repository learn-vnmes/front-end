// swiftlint:disable all
// Generated using SwiftGen — https://github.com/SwiftGen/SwiftGen

#if os(OSX)
  import AppKit.NSImage
  internal typealias AssetColorTypeAlias = NSColor
  internal typealias AssetImageTypeAlias = NSImage
#elseif os(iOS) || os(tvOS) || os(watchOS)
  import UIKit.UIImage
  internal typealias AssetColorTypeAlias = UIColor
  internal typealias AssetImageTypeAlias = UIImage
#endif

// swiftlint:disable superfluous_disable_command
// swiftlint:disable file_length

// MARK: - Asset Catalogs

// swiftlint:disable identifier_name line_length nesting type_body_length type_name
internal enum Asset {
  internal enum Assets {
  }
  internal enum Login {
    internal static let arm = ImageAsset(name: "Arm")
    internal static let body = ImageAsset(name: "Body")
    internal static let ear = ImageAsset(name: "Ear")
    internal static let eyeDoe = ImageAsset(name: "Eye-doe")
    internal static let eye = ImageAsset(name: "Eye")
    internal static let head = ImageAsset(name: "Head")
    internal static let loginIcPlaceholderBackgroud = ImageAsset(name: "Login_icPlaceholder_Backgroud")
    internal static let mouthCircle = ImageAsset(name: "Mouth-circle")
    internal static let mouthClosed = ImageAsset(name: "Mouth-closed")
    internal static let mouthFull = ImageAsset(name: "Mouth-full")
    internal static let mouthHalf = ImageAsset(name: "Mouth-half")
    internal static let mouthSmile = ImageAsset(name: "Mouth-smile")
    internal static let muzzle = ImageAsset(name: "Muzzle")
    internal static let nose = ImageAsset(name: "Nose")
    internal static let passwordHide = ImageAsset(name: "Password-hide")
    internal static let passwordShow = ImageAsset(name: "Password-show")
    internal static let active = ImageAsset(name: "active")
    internal static let banner = ImageAsset(name: "banner")
    internal static let bgSunnyCloud1 = ImageAsset(name: "bg-sunny-cloud-1")
    internal static let bgSunnyCloud2 = ImageAsset(name: "bg-sunny-cloud-2")
    internal static let bgSunnyCloud3 = ImageAsset(name: "bg-sunny-cloud-3")
    internal static let bgSunnyCloud4 = ImageAsset(name: "bg-sunny-cloud-4")
    internal static let demo = DataAsset(name: "demo")
    internal static let ecstatic = ImageAsset(name: "ecstatic")
    internal static let loginCritter = DataAsset(name: "login-critter")
    internal static let neutral = ImageAsset(name: "neutral")
    internal static let peek = ImageAsset(name: "peek")
    internal static let shy = ImageAsset(name: "shy")
  }
}
// swiftlint:enable identifier_name line_length nesting type_body_length type_name

// MARK: - Implementation Details

internal struct ColorAsset {
  internal fileprivate(set) var name: String

  @available(iOS 11.0, tvOS 11.0, watchOS 4.0, OSX 10.13, *)
  internal var color: AssetColorTypeAlias {
    return AssetColorTypeAlias(asset: self)
  }
}

internal extension AssetColorTypeAlias {
  @available(iOS 11.0, tvOS 11.0, watchOS 4.0, OSX 10.13, *)
  convenience init!(asset: ColorAsset) {
    let bundle = Bundle(for: BundleToken.self)
    #if os(iOS) || os(tvOS)
    self.init(named: asset.name, in: bundle, compatibleWith: nil)
    #elseif os(OSX)
    self.init(named: NSColor.Name(asset.name), bundle: bundle)
    #elseif os(watchOS)
    self.init(named: asset.name)
    #endif
  }
}

internal struct DataAsset {
  internal fileprivate(set) var name: String

  #if os(iOS) || os(tvOS) || os(OSX)
  @available(iOS 9.0, tvOS 9.0, OSX 10.11, *)
  internal var data: NSDataAsset {
    return NSDataAsset(asset: self)
  }
  #endif
}

#if os(iOS) || os(tvOS) || os(OSX)
@available(iOS 9.0, tvOS 9.0, OSX 10.11, *)
internal extension NSDataAsset {
  convenience init!(asset: DataAsset) {
    let bundle = Bundle(for: BundleToken.self)
    #if os(iOS) || os(tvOS)
    self.init(name: asset.name, bundle: bundle)
    #elseif os(OSX)
    self.init(name: NSDataAsset.Name(asset.name), bundle: bundle)
    #endif
  }
}
#endif

internal struct ImageAsset {
  internal fileprivate(set) var name: String

  internal var image: AssetImageTypeAlias {
    let bundle = Bundle(for: BundleToken.self)
    #if os(iOS) || os(tvOS)
    let image = AssetImageTypeAlias(named: name, in: bundle, compatibleWith: nil)
    #elseif os(OSX)
    let image = bundle.image(forResource: NSImage.Name(name))
    #elseif os(watchOS)
    let image = AssetImageTypeAlias(named: name)
    #endif
    guard let result = image else { fatalError("Unable to load image named \(name).") }
    return result
  }
}

internal extension AssetImageTypeAlias {
  @available(iOS 1.0, tvOS 1.0, watchOS 1.0, *)
  @available(OSX, deprecated,
    message: "This initializer is unsafe on macOS, please use the ImageAsset.image property")
  convenience init!(asset: ImageAsset) {
    #if os(iOS) || os(tvOS)
    let bundle = Bundle(for: BundleToken.self)
    self.init(named: asset.name, in: bundle, compatibleWith: nil)
    #elseif os(OSX)
    self.init(named: NSImage.Name(asset.name))
    #elseif os(watchOS)
    self.init(named: asset.name)
    #endif
  }
}

private final class BundleToken {}
