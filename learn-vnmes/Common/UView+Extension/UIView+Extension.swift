//
//  UIView+Extension.swift
//  learn-vnmes
//
//  Created by Pham Anh Tuan on 2/22/20.
//  Copyright © 2020 Pham Anh Tuan. All rights reserved.
//

import Foundation
import UIKit

extension UIView {

    public func removeShadow() {
        dropShadow(color: .gray, offset: CGSize(width: 0, height: 0), opacity: 0.0, radius: 0)
    }
    public func dropCardShadow() {
        dropShadow(color: .gray, offset: CGSize(width: 0, height: 4), opacity: 0.1, radius: 4)
    }
    
    public func dropGlowShadow() {
        dropShadow(color: .gray, offset: CGSize(width: 0, height: 2), opacity: 0.5, radius: 4)
    }
    
    public func drawCardBorder() {
        drawBorder(color: .gray)
    }
    
    public func dropShadow(color: UIColor = UIColor.black,
                           offset: CGSize = CGSize.zero,
                           opacity: Float = 1.0,
                           radius: CGFloat = 0.0) {

        layer.shadowColor = color.cgColor
        layer.shadowOffset = offset
        layer.shadowOpacity = opacity
        layer.shadowRadius = radius
    }
    
    public func drawBorder(color: UIColor = .white, width: CGFloat = 0.5) {
        layer.borderColor = color.cgColor
        layer.borderWidth = width
    }
    
    public func drawBorder(edge: UIRectEdge, color: UIColor = .white, width: CGFloat = 0.5) {
        
        let border = CALayer()
        
        switch edge {
        case UIRectEdge.top:
            border.frame = CGRect(x: 0, y: 0, width: frame.width, height: width)
            
        case UIRectEdge.bottom:
            border.frame = CGRect(x: 0, y: frame.height - width, width: frame.width, height: width)
            
        case UIRectEdge.left:
            border.frame = CGRect(x: 0, y: 0, width: width, height: frame.height)
            
        case UIRectEdge.right:
            border.frame = CGRect(x: frame.width - width, y: 0, width: width, height: frame.height)
            
        default: break
        }
        
        border.backgroundColor = color.cgColor
        layer.addSublayer(border)
    }
    
    public func setCorner(radius: CGFloat = 4.0,
                          corners: UIRectCorner = [.allCorners],
                          backgroundColor fillColor: UIColor) {

        backgroundColor = UIColor.clear
        
        UIGraphicsBeginImageContextWithOptions(self.frame.size, false, UIScreen.main.scale)
        
        let context = UIGraphicsGetCurrentContext()
        let cornerSize = CGSize(width: radius, height: radius)
        let bezierPath = UIBezierPath(roundedRect: bounds, byRoundingCorners: corners, cornerRadii: cornerSize)
        
        context?.addPath(bezierPath.cgPath)
        context?.setFillColor(fillColor.cgColor)
        context?.closePath()
        context?.fillPath()
        
        let currentImage = UIGraphicsGetImageFromCurrentImageContext()
        self.layer.contents = currentImage?.cgImage
        
        UIGraphicsEndImageContext()
    }
    
    public func setCircle(backgroundColor: UIColor) {
        setCorner(radius: bounds.width / 2, backgroundColor: backgroundColor)
    }

    public var width: CGFloat {
        get {
            return frame.size.width
        }
        set {
            frame.size.width = newValue
        }
    }

    public var height: CGFloat {
        get {
            return frame.size.height
        }
        set {
            frame.size.height = newValue
        }
    }

    public var size: CGSize {
        get {
            return frame.size
        }
        set {
            width = newValue.width
            height = newValue.height
        }
    }
    
    public func roundCorners(_ corners: UIRectCorner, radius: CGFloat) {
        let path = UIBezierPath(roundedRect: self.bounds,
                                byRoundingCorners: corners,
                                cornerRadii: CGSize(width: radius, height: radius))
        let mask = CAShapeLayer()
        mask.frame = self.bounds
        mask.path = path.cgPath
        self.layer.mask = mask
    }
}
