//
//  TakeWhen.swift
//  learn-vnmes
//
//  Created by Pham Anh Tuan on 11/25/19.
//  Copyright © 2019 Pham Anh Tuan. All rights reserved.
//
import Foundation
import RxCocoa
import RxSwift

extension ObservableType {
    
    /// Returns the latest element from the source observable sequence until the other observable sequence produces `true` element.
    ///
    /// - note: Only the latest value from this observable is emitted
    ///
    /// - parameter other: Observable sequence that filter elements of the source sequence.
    /// - returns: An observable sequence containing the elements of the source sequence
    
    func takeLatestWhen(_ other: Observable<Bool>) -> Observable<E> {
        return Observable
            .combineLatest(other, self) { ($0, $1) }
            .filter { other, _ in return other }
            .map { _, value in return value }
    }
}

extension SharedSequenceConvertibleType where SharingStrategy == DriverSharingStrategy {
    
    /// Returns the latest element from the source observable sequence until the other observable sequence produces `true` element.
    ///
    /// - note: Only the latest value from this observable is emitted
    ///
    /// - parameter other: Observable sequence that filter elements of the source sequence.
    /// - returns: An observable sequence containing the elements of the source sequence
    
    func takeLatestWhen(_ other: Driver<Bool>) -> Driver<E> {
        return Driver
            .combineLatest(other, self) { ($0, $1) }
            .filter { other, _ in return other }
            .map { _, value in return value }
    }
}
