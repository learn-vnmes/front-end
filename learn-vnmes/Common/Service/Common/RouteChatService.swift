//
//  RouteChatService.swift
//  learn-vnmes
//
//  Created by Pham Anh Tuan on 2/11/20.
//  Copyright © 2020 Pham Anh Tuan. All rights reserved.
//
import Action
import Foundation
import RxSwift
import RxCocoa

import Dispatch
import SwiftGRPC
import SwiftProtobuf

import SwiftGRPCClient



class ProfileServiceSesion: Session  {
    static let shared = Session(address: "127.0.0.1:9090", secure: false )
}

protocol RouteChatServiceType {
    func fetchListFeatures(hiLong: Int32, hiLat: Int32, loLong: Int32, loLat: Int32) -> Observable<[V1_Feature]>
}
//
class GRPCRouteChatService: RouteChatServiceType {


    func fetchListFeatures(hiLong: Int32, hiLat: Int32, loLong: Int32, loLat: Int32) -> Observable<[V1_Feature]> {
        return Observable.just([])
    }


}
