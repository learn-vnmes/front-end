//
//  RecordRouteRequest.swift
//  learn-vnmes
//
//  Created by Pham Anh Tuan on 2/19/20.
//  Copyright © 2020 Pham Anh Tuan. All rights reserved.
//

import Foundation

struct RecordRouteRequest: V1_ProfileServiceRecordRouteRequest {
    var request: V1_Point = V1_Point()
    
    init() {
        request.longitude = 10000
        request.latitude = 200000
    }
}
