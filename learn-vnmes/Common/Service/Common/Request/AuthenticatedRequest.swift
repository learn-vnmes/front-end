//
//  AuthenticationRequest.swift
//  learn-vnmes
//
//  Created by Pham Anh Tuan on 3/1/20.
//  Copyright © 2020 Pham Anh Tuan. All rights reserved.
//

import SwiftGRPC
import SwiftGRPCClient
import Foundation

protocol AuthenticatedRequest: Request {}

extension AuthenticatedRequest where Self : Request  {
    var token: String {
        UserDefaults.standard.authenticationToken
    }
    
    func intercept(metadata: Metadata) throws -> Metadata {
        try metadata.add(key: "authorization", value: self.token)
        return metadata
    }
}
