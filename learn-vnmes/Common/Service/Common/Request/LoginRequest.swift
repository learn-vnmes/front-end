//
//  LoginRequest.swift
//  learn-vnmes
//
//  Created by Pham Anh Tuan on 2/23/20.
//  Copyright © 2020 Pham Anh Tuan. All rights reserved.
//

import Foundation

struct LoginRequest: V1_ProfileServiceLoginRequest {
    var request: V1_LoginRequest = V1_LoginRequest()
    
    init(username: String, password: String) {
        request.username = username
        request.password = password
    }
}
