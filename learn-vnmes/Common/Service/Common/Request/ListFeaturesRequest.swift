//
//  ListFeaturesRequest.swift
//  learn-vnmes
//
//  Created by Pham Anh Tuan on 2/18/20.
//  Copyright © 2020 Pham Anh Tuan. All rights reserved.
//

import Foundation

struct ListFeaturesRequest: V1_ProfileServiceListFeaturesRequest {
    var request =  V1_Rectangle()
    
    init(hiLong: Int32, hiLat: Int32, loLong: Int32, loLat: Int32) {
        request.hi = V1_Point.with {
            $0.longitude = hiLong
            $0.latitude = hiLat
        }
        
        request.lo = V1_Point.with {
            $0.longitude = loLong
            $0.latitude = loLat
        }
    }
    
    func buildRequest() -> V1_Rectangle {
        return request
    }
}
