//
//  GetFeatureRequest.swift
//  learn-vnmes
//
//  Created by Pham Anh Tuan on 2/19/20.
//  Copyright © 2020 Pham Anh Tuan. All rights reserved.
//

import Foundation

struct GetFeatureRequest: V1_ProfileServiceGetFeatureRequest {
    
    var request: V1_Point = V1_Point()
    init() {
        request = V1_Point.with {
            $0.latitude = 407838351
            $0.longitude = -746143763
        }
    }
}
