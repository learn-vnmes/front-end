//
//  SendMessageRequest.swift
//  learn-vnmes
//
//  Created by Pham Anh Tuan on 3/1/20.
//  Copyright © 2020 Pham Anh Tuan. All rights reserved.
//

import Foundation
import SwiftGRPCClient

struct SendMessageRequest: V1_FilmsServiceSendMessageRequest, AuthenticatedRequest {
    var request: V1_ChatStreamMessage = V1_ChatStreamMessage()
    
    func buildRequest(_ message: (String, String)) -> InputType {
        var request = self.request
        request.message = message.0
        request.name = message.1
        return request
    }
}
