//
//  ListFilmCategoriesRequest.swift
//  learn-vnmes
//
//  Created by Pham Anh Tuan on 3/1/20.
//  Copyright © 2020 Pham Anh Tuan. All rights reserved.
//
import Foundation

struct ListFilmCategoriesRequest: V1_FilmsServiceListFashionCategoriesRequest, AuthenticatedRequest {}

