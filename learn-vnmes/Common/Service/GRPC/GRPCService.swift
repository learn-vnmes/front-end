import Foundation
import JWTDecode
import ObjectMapper
import RxSwift

//public class GRPCClient: GRPCClientType {
//    public static let `default`: GRPCClient = {
//        GRPCClient()
//    }()
//    
//    
//    
//    
////
////    private let tokenProvider: GRPCAccessTokenProviderType
////
////    init(tokenProvider: GRPCAccessTokenProviderType = GRPCAccessTokenProvider.default) {
////        self.tokenProvider = tokenProvider
////    }
//}
//
//public protocol GRPCClientType {
//    
//}



//
//protocol GRPCAccessTokenProviderType {
//
//    var currentToken: JWTToken { get }
//    var observableCurrentToken: Observable<JWTToken> { get }
//    var tokenHasInvalid: Observable<Bool> { get}
//
//    var refreshErrors: Observable<Error> { get }
//    var refreshing: Observable<Bool> { get }
//
//    func clearToken()
//    func store(token: JWTToken)
//    func invalidateToken()
//
//
//    func refreshTokenIfNeeded()
//    func issuedDate(forAccessToken token: String) -> Date?
//}
//
//extension GRPCAccessTokenProviderType {
//    func issuedDate(forAccessToken token: String) -> Date? {
//        guard let jwt = try? decode(jwt: token) else { return nil}
//        return jwt.issuedAt
//    }
//}
//
//public class GRPCAccessTokenProvider: GRPCAccessTokenProviderType {
//
//    public static let `default` = GRPCAccessTokenProvider()
//    var currentToken: JWTToken
//
//    var observableCurrentToken: Observable<JWTToken>
//
//    var tokenHasInvalid: Observable<Bool>
//
//    var refreshErrors: Observable<Error>
//
//    var refreshing: Observable<Bool>
//
//    func clearToken() {
//
//    }
//
//    func store(token: JWTToken) {
//
//    }
//
//    func invalidateToken() {
//
//    }
//
//    func refreshTokenIfNeeded() {
//
//    }
//
//    init() {
//
//    }
//
//
//}


public struct JWTToken: Codable, Equatable {

    public static let empty: JWTToken = JWTToken(type: "",
                                                 accessToken: "",
                                                 refreshToken: "",
                                                 issuedDate: Date(timeIntervalSince1970: 0))

    public let type: String
    public let accessToken: String
    public let refreshToken: String
    public private(set) var issuedDate: Date
    
    public var isAccessTokenValid: Bool { return !accessToken.isEmpty }
    public var isRefreshTokenValid: Bool { return !refreshToken.isEmpty }
    
    private enum CodingKeys: String, CodingKey {
        case type
        case accessToken
        case refreshToken
        case issuedDate
    }

    public init(type: String,
                accessToken: String,
                refreshToken: String,
                issuedDate: Date) {
    
        self.type = type
        self.accessToken = accessToken
        self.refreshToken = refreshToken
        self.issuedDate = issuedDate
    }
    
    public init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        type = try container.decode(String.self, forKey: .type)
        accessToken = try container.decode(String.self, forKey: .accessToken)
        refreshToken = try container.decode(String.self, forKey: .refreshToken)
        
        do {
            issuedDate = try container.decode(Date.self, forKey: .issuedDate)
        } catch {
            if accessToken.isEmpty {
                issuedDate = JWTToken.empty.issuedDate
                return
            }
            
            let info = JWTToken.decodeAccessToken(accessToken)
            issuedDate = info.issuedAt
        }
    }
    
    private static func decodeAccessToken(_ token: String) -> (expiresAt: Date, issuedAt: Date) {
        guard
            let jwt = try? decode(jwt: token),
            let expiresAt = jwt.expiresAt,
            let issuedAt = jwt.issuedAt
        else {
            fatalError("Cannot decode JWT. Access Token is invalid")
        }
        return (expiresAt, issuedAt)
    }
}

extension JWTToken: ImmutableMappable {

    public init(map: Map) throws {
        accessToken = try map.value("access_token")
        refreshToken = try map.value("refresh_token")
        type = try map.value("token_type")
        
        let info = JWTToken.decodeAccessToken(accessToken)
        issuedDate = info.issuedAt
    }
}

