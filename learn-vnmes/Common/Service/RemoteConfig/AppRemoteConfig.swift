//
//  LoginRemoteConfig.swift
//  learn-vnmes
//
//  Created by Pham Anh Tuan on 11/24/19.
//  Copyright © 2019 Pham Anh Tuan. All rights reserved.
//

import Foundation
import Firebase
import ObjectMapper
import RxCocoa
import RxSwift
import RxOptional

public extension Optional {

    func `or`(_ value: Wrapped?) -> Optional {
        return self ?? value
    }

    func `or`(_ value: Wrapped) -> Wrapped {
        return self ?? value
    }
}
public class FBRemoteConfig {
    public static let `default`: FBRemoteConfig = FBRemoteConfig()
    public let remoteConfig: RemoteConfig
    public var didFetchAndActive: Observable<Bool> {
        return didFetchAndActiveRelay.asObservable().observeOn(MainScheduler.instance)
    }
    
    private var didFetchAndActiveRelay = BehaviorRelay<Bool>(value: false)
    
    public init(){
        if FirebaseApp.app() == nil {
            FirebaseApp.configure()
        }
        
        guard let app = FirebaseApp.app() else {
            fatalError("♨️♨️♨️[FIREBASE REMOTE CONFIG] Could not config firebase app")
        }
        
        remoteConfig = RemoteConfig.remoteConfig(app: app)
        let settings = RemoteConfigSettings()
        settings.minimumFetchInterval = 15 * 60
        remoteConfig.configSettings = settings
        remoteConfig.ensureInitialized { [weak self] error in
            guard let self = self else { return }
            self.fetchData()
        }
    }
    
    private func fetchData() {
        remoteConfig.activate { [weak self] _ in
            guard let self = self else { return }
            self.didFetchAndActiveRelay.accept(true)
        }
        
        remoteConfig.fetchAndActivate { [weak self] (_,_)in
            guard let self = self else { return }
            self.didFetchAndActiveRelay.accept(true)
        }
    }
}


public protocol AppRemoteConfigManagerType: AnyObject {
    func bool(forKey key: String) -> Bool
    func bool(forKey key: String, defaultValue: Bool) -> Bool
    func integer(forKey key: String, defaultValue: Int) -> Int
    func integer(forKey key: String, defaultValue: Int, condition: (Int) -> Bool) -> Int
    func double(forKey key: String, defaultValue: Double) -> Double
    func double(forKey key: String, defaultValue: Double, condition: (Double) -> Bool) -> Double
    func string(forKey key: String) -> String?
    func string(forKey key: String, defaultValue: String) -> String
    func string(forKey key: String, defaultValue: String, replaceEmpty: Bool) -> String
    func url(forKey key: String) -> URL?
    func url(forKey key: String, defaultValue: URL) -> URL
    func codableObject<T: Decodable>(withType type: T.Type, key: String) -> T?
    func codableObject<T: Decodable>(forKey key: String) -> T?
    func codableObject<T: Decodable>(forKey key: String, defaultValue: T) -> T
    func codableArray<T: Decodable>(forKey key: String, defaultValue: [T]) -> [T]
    func mappableObject<T: BaseMappable>(withType type: T.Type, key: String) -> T?
    func mappableObject<T: BaseMappable>(forKey key: String) -> T?
    func mappableObject<T: BaseMappable>(forKey key: String, defaultValue: T) -> T
    func mappableArray<T: BaseMappable>(forKey key: String, defaultValue: [T]) -> [T]
}

public class AppRemoteConfigManager: AppRemoteConfigManagerType {
    
    public static let `default`: AppRemoteConfigManagerType = AppRemoteConfigManager()
    private let fbRemoteConfig: FBRemoteConfig
    private var remoteConfig: RemoteConfig{ return fbRemoteConfig.remoteConfig}
    
    private init(remoteConfig: FBRemoteConfig = FBRemoteConfig.default) {
        self.fbRemoteConfig = remoteConfig
    }

    public func bool(forKey key: String) -> Bool {
        bool(forKey: key, defaultValue: false)
    }

    public func bool(forKey key: String, defaultValue: Bool) -> Bool {
        let string = remoteConfig.configValue(forKey: key).stringValue
        if string == "true" {
            return true
        } else {
            return defaultValue
        }
    }

    public func integer(forKey key: String, defaultValue: Int) -> Int {
        (remoteConfig.configValue(forKey: key).numberValue?.intValue).or(defaultValue)
    }

    public func integer(forKey key: String, defaultValue: Int, condition: (Int) -> Bool) -> Int {
        let value = (remoteConfig.configValue(forKey: key).numberValue?.intValue).or(defaultValue)
        return condition(value) ? value : defaultValue
    }

    public func double(forKey key: String, defaultValue: Double) -> Double {
        (remoteConfig.configValue(forKey: key).numberValue?.doubleValue).or(defaultValue)
    }

    public func double(forKey key: String, defaultValue: Double, condition: (Double) -> Bool) -> Double {
        let value = (remoteConfig.configValue(forKey: key).numberValue?.doubleValue).or(defaultValue)
        return condition(value) ? value : defaultValue
    }

    public func string(forKey key: String) -> String? {
        remoteConfig.configValue(forKey: key).stringValue
    }

    public func string(forKey key: String, defaultValue: String) -> String {
        string(forKey: key).or(defaultValue)
    }

    public func string(forKey key: String, defaultValue: String, replaceEmpty: Bool) -> String {
        if replaceEmpty {
            return string(forKey: key).or(defaultValue)
        }
        return string(forKey: key).or(defaultValue)
    }

    public func url(forKey key: String) -> URL? {
        remoteConfig.configValue(forKey: key).stringValue.flatMap(URL.init)
    }

    public func url(forKey key: String, defaultValue: URL) -> URL {
        url(forKey: key).or(defaultValue)
    }

    public func codableObject<T: Decodable>(forKey key: String) -> T? {
        let data = remoteConfig.configValue(forKey: key).dataValue
        return try? JSONDecoder().decode(T.self, from: data)
    }

    public func codableObject<T: Decodable>(withType type: T.Type, key: String) -> T? {
        codableObject(forKey: key)
    }

    public func codableObject<T: Decodable>(forKey key: String, defaultValue: T) -> T {
        codableObject(forKey: key).or(defaultValue)
    }

    public func codableArray<T: Decodable>(forKey key: String, defaultValue: [T]) -> [T] {
        let data = remoteConfig.configValue(forKey: key).dataValue
        return (try? JSONDecoder().decode([T].self, from: data)).or(defaultValue)
    }

    public func mappableObject<T: BaseMappable>(forKey key: String) -> T? {
        let data = remoteConfig.configValue(forKey: key).dataValue
        guard let dict = try? JSONSerialization.jsonObject(with: data, options: []) as? [String: Any] else {
            return nil
        }
        return T(JSON: dict)
    }

    public func mappableObject<T: BaseMappable>(withType type: T.Type, key: String) -> T? {
        mappableObject(forKey: key)
    }

    public func mappableObject<T: BaseMappable>(forKey key: String, defaultValue: T) -> T {
        mappableObject(forKey: key).or(defaultValue)
    }

    public func mappableArray<T: BaseMappable>(forKey key: String, defaultValue: [T]) -> [T] {
        let data = remoteConfig.configValue(forKey: key).dataValue
        guard let dicts = try? JSONSerialization.jsonObject(with: data, options: []) as? [[String: Any]] else {
            return defaultValue
        }
        return dicts.compactMap { T(JSON: $0) }
    }
}
