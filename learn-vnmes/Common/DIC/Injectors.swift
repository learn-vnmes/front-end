//
//  Injectors.swift
//  learn-vnmes
//
//  Created by Pham Anh Tuan on 2/12/20.
//  Copyright © 2020 Pham Anh Tuan. All rights reserved.
//

import Foundation


class Injectors {
    
     private static var injector: Injector!
       
       static func initialize(modules: Module...){
           self.injector = Injector.builder()
               .withBinder(binder: Binder())
               .withModules(modules)
               .build()
       }
       
       static func get<T>(_ type: T.Type, name: String = "") -> T {
           guard let instance = injector.get(type, name: name) else {
               fatalError("\(type) has no instance provided")
           }
           return instance
       }
}


