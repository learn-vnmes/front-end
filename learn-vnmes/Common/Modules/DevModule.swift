//
//  DevModule.swift
//  learn-vnmes
//
//  Created by Pham Anh Tuan on 2/11/20.
//  Copyright © 2020 Pham Anh Tuan. All rights reserved.
//

import Foundation

public enum ServiceType: String {
    case http
    case grpc
}

public enum HostType: String{
    case apiHost
    case grpcHost
}

class DevModule: AbstractModule {
    override func configure() {
        super.configure()
        
        bind(type: RouteChatServiceType.self)
            .withName(name: ServiceType.grpc.rawValue)
            .to(instance: GRPCRouteChatService())
    }
}

