//
//  Userdefaults+Token.swift
//  learn-vnmes
//
//  Created by Pham Anh Tuan on 3/1/20.
//  Copyright © 2020 Pham Anh Tuan. All rights reserved.
//

import Foundation
enum DefaultKeys {
    static let authenticationToken = "authentication_token"
}

extension UserDefaults {
    var authenticationToken: String {
        get {
            return string(forKey: DefaultKeys.authenticationToken) ?? ""
        }
        set {
            setValue(newValue, forKey: DefaultKeys.authenticationToken)
        }
    }
}

