//
//  Streaming+Rx.swift
//  Echo
//
//  Created by Duy Tran on 2/13/20.
//

import RxSwift
import RxCocoa
import SwiftGRPCClient

extension Streaming where Self.Message == Self.Request.Message, Self.Request : SwiftGRPCClient.SendRequest {

    func send(message: Message) -> Observable<Void> {
        .create { [weak self] (observer) -> Disposable in
            guard let self = self else { return Disposables.create() }

            self.send(message) { (result) in
                switch result {
                case .success:
                    observer.onNext(())
                case let .failure(error):
                    observer.onError(error)
                }
            }

            return Disposables.create()
        }
    }
}

extension Streaming where Self.Request : SwiftGRPCClient.ReceiveRequest {

    func create() -> Observable<Self.Request.OutputType> {
        .create { [weak self] (observer) -> Disposable in
            guard let self = self else { return Disposables.create() }

            self.receive { (result) in
                switch result {
                case let .success(response):
                    observer.onNext(response)
                case let .failure(error):
                    observer.onError(error)
                }
            }

            return Disposables.create()
        }
    }
}

extension Streaming where Self.Request: SwiftGRPCClient.UnaryRequest {
    func execute() -> Observable<Self.Request.OutputType> {
        .create { [weak self] (observer)-> Disposable in
            guard let self = self else { return Disposables.create()}
            self.data { (result) in
                switch result {
                case let .success(response):
                    observer.onNext(response)
                case let .failure(error):
                    observer.onError(error)
                }
            }
            
            return Disposables.create()
        }
    }
}


extension Streaming where Self.Request : SwiftGRPCClient.CloseRequest {

    func close() -> Observable<Void> {
        .create { [weak self] (observer) -> Disposable in
            guard let self = self else { return Disposables.create() }

            self.close() { (result) in
                switch result {
                case .success:
                    observer.onNext(())
                case let .failure(error):
                    observer.onError(error)
                }
            }

            return Disposables.create()
        }
    }
}

extension Streaming where Self.Request : SwiftGRPCClient.CloseAndReciveRequest {

    func closeAndReceive() -> Observable<Self.Request.OutputType> {
        .create { [weak self] (observer) -> Disposable in
            guard let self = self else { return Disposables.create() }

            self.closeAndReceive { (result) in
                switch result {
                case let .success(response):
                    observer.onNext(response)
                case let .failure(error):
                    observer.onError(error)
                }
            }

            return Disposables.create()
        }
    }
}
