//
//  MessageCell.swift
//  learn-vnmes
//
//  Created by Pham Anh Tuan on 3/1/20.
//  Copyright © 2020 Pham Anh Tuan. All rights reserved.
//

import UIKit
import Reusable
import RxSwift
import RxCocoa

class MessageCell: UITableViewCell, NibReusable {
    

    @IBOutlet weak var contentLabel: UILabel!
    @IBOutlet weak var leftContentConstraint: NSLayoutConstraint!
    @IBOutlet weak var rightContentConstrait: NSLayoutConstraint!
    @IBOutlet weak var contentMessageView: UIView!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        configureViews()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func configureCell(viewModel: MessageViewModel) {
        contentLabel.text = viewModel.message
        
        switch viewModel.model.type {
        case .server:
            contentLabel.textAlignment = .left
            rightContentConstrait.constant = 80
            leftContentConstraint.constant = 16
            contentMessageView.backgroundColor = #colorLiteral(red: 0.8039215803, green: 0.8039215803, blue: 0.8039215803, alpha: 1)
            contentLabel.textColor = UIColor.black
        case .user:
            leftContentConstraint.constant = 80
            rightContentConstrait.constant = 16
            contentMessageView.backgroundColor = #colorLiteral(red: 1, green: 0.1857388616, blue: 0.5733950138, alpha: 1)
            contentLabel.textColor = UIColor.white
            contentLabel.textAlignment = .right
            
        }
    }
    
    private func configureViews() {
        configureMessageContentView()
    }
    
    private func configureMessageContentView() {
        contentMessageView.layer.cornerRadius = 12.0
        contentMessageView.layer.borderColor = UIColor.gray.cgColor
        contentMessageView.layer.borderWidth = 0.25
    }
}
