//
//  ChatRouter.swift
//  learn-vnmes
//
//  Created by Pham Anh Tuan on 3/1/20.
//  Copyright © 2020 Pham Anh Tuan. All rights reserved.
//

import RIBs

protocol ChatInteractable: Interactable {
    var router: ChatRouting? { get set }
    var listener: ChatListener? { get set }
}

protocol ChatViewControllable: ViewControllable {
    func present(viewController: ViewControllable)
      func dismiss(viewController: ViewControllable)
}

final class ChatRouter: ViewableRouter<ChatInteractable, ChatViewControllable>, ChatRouting {

    // TODO: Constructor inject child builder protocols to allow building children.
    override init(interactor: ChatInteractable, viewController: ChatViewControllable) {
        super.init(interactor: interactor, viewController: viewController)
        interactor.router = self
    }
}
