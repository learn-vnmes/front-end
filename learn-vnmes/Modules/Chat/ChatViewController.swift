//
//  ChatViewController.swift
//  learn-vnmes
//
//  Created by Pham Anh Tuan on 3/1/20.
//  Copyright © 2020 Pham Anh Tuan. All rights reserved.
//

import RIBs
import RxSwift
import UIKit
import Reusable
import RxDataSources
import RxCocoa

protocol ChatPresentableListener: class {
    var exitTrigger: PublishRelay<Void> { get }
    var sendMessageTriger: PublishRelay<String> { get }
}

struct MessageViewModel {
    
    let message: String
    let model: ChatMessage
    
    init(model: ChatMessage) {
        self.message = model.message
        self.model = model
    }
}

final class ChatViewController: UIViewController, ChatPresentable, ChatViewControllable, NibOwnerLoadable {
    typealias Section = SectionModel<String, MessageViewModel>
    typealias DataSource = RxTableViewSectionedReloadDataSource<Section>
    lazy var dataSource = makeDatasource()
    

    var messages: BehaviorRelay<[ChatMessage]> = BehaviorRelay(value: [])
    
    weak var listener: ChatPresentableListener?
    private let disposeBag = DisposeBag()
    
    @IBOutlet weak var exitButton: UIButton!
    @IBOutlet weak var sendMsgButton: UIButton!
    @IBOutlet weak var messageContentTextView: UITextView!
    @IBOutlet weak var tableView: UITableView!
    
    func present(viewController: ViewControllable) {
            viewController.uiviewController.modalPresentationStyle = .fullScreen
            present(viewController.uiviewController, animated: true, completion: nil)
        }
        
        func dismiss(viewController: ViewControllable) {
              if presentedViewController === viewController.uiviewController {
                  dismiss(animated: true, completion: nil)
              }
          }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configureViews()
    }
}

extension ChatViewController {
    private func makeDatasource() -> DataSource {
        let configureCell: DataSource.ConfigureCell = { (_, tableView, indexPath, viewModel) -> UITableViewCell in
                   let cell: MessageCell = tableView.dequeueReusableCell(for: indexPath)
                   cell.configureCell(viewModel: viewModel)
                   return cell
               }
        
        return DataSource(configureCell: configureCell)
    }
}

extension ChatViewController {
    private func configureViews() {
        configureExitButton()
        configureMessageContentTextView()
        configureSendMessageButton()
        configureTableView()
    }
    
    private func configureExitButton() {
        exitButton.rx.tap.subscribeNext {[weak self]  in
            guard let self = self else { return }
            self.dismiss(animated: true, completion: nil)
//            self.listener?.exitTrigger.accept(())
        }.disposed(by: disposeBag)
    }
    
    private func configureMessageContentTextView() {
        messageContentTextView.layer.cornerRadius = 8
        messageContentTextView.layer.borderColor = UIColor.gray.cgColor
        messageContentTextView.layer.borderWidth =  0.5
        
        messageContentTextView
            .rx
            .text
            .map{ text -> Bool in
                guard let content = text else { return false }
                return content.isNotEmpty
            }
            .bind(to: sendMsgButton.rx.isEnabled)
            .disposed(by: disposeBag)
    }
    
    private func configureSendMessageButton() {
        sendMsgButton.rx.tap.subscribeNext { [weak self] _ in
            guard let self = self else { return }
            guard let message = self.messageContentTextView.text else { return }
            self.messageContentTextView.text = ""
            self.listener?.sendMessageTriger.accept(message)
        }.disposed(by: disposeBag)
    }
    
    private func configureTableView() {
        
        tableView.register(cellType: MessageCell.self)
        tableView.estimatedRowHeight = 44
        tableView.rowHeight = UITableView.automaticDimension
        tableView.sectionHeaderHeight = 16
        
        messages
            .asDriver()
            .map { messages -> [Section] in
                var items: [MessageViewModel] = []
                messages.forEach { message in
                    items.append(MessageViewModel(model: message))
                }
            
                return [Section(model: "message_section", items: items)]
            }
            .drive(tableView.rx.items(dataSource: dataSource))
            .disposed(by: disposeBag)
        
    }
}
