//
//  ChatInteractor.swift
//  learn-vnmes
//
//  Created by Pham Anh Tuan on 3/1/20.
//  Copyright © 2020 Pham Anh Tuan. All rights reserved.
//

import RIBs
import RxSwift
import RxCocoa

protocol ChatRouting: ViewableRouting {
    
}

enum MessageType: Int {
    case user
    case server
}

struct ChatMessage {
    let type: MessageType
    let message: String
    
    init(type: MessageType, message: String = "") {
        self.type = type
        self.message = message
    }
}

protocol ChatPresentable: Presentable {
    var listener: ChatPresentableListener? { get set }
    var messages: BehaviorRelay<[ChatMessage]> { get set }
}

protocol ChatListener: class {
    func detachChat()
}

final class ChatInteractor: PresentableInteractor<ChatPresentable>, ChatInteractable, ChatPresentableListener {
    var exitTrigger: PublishRelay<Void> = PublishRelay()
    var sendMessageTriger: PublishRelay<String> = PublishRelay()
    
    var sendMessageStreamingRelay = FilmService.shared.stream(with: SendMessageRequest())
    

    weak var router: ChatRouting?
    weak var listener: ChatListener?

    // TODO: Add additional dependencies to constructor. Do not perform any logic
    // in constructor.
    override init(presenter: ChatPresentable) {
        super.init(presenter: presenter)
        presenter.listener = self
    }

    override func didBecomeActive() {
        super.didBecomeActive()
        configureListener()
    }

    override func willResignActive() {
        super.willResignActive()
        sendMessageStreamingRelay.cancel()
        // TODO: Pause any business logic.
    }
}

extension ChatInteractor {
    private func configureListener() {
        exitTrigger
            .subscribeNext { [weak self] _ in
                guard let self = self else { return }
                self.listener?.detachChat()
            }
            .disposeOnDeactivate(interactor: self)
        
        sendMessageStreamingRelay.receive { result in
            switch result {
                case .failure(_):
                    break
                case let .success(data):
                    let messages = self.presenter.messages.value + [ChatMessage(type: .server, message: data.message)]
                    self.presenter.messages.accept(messages)
                }
        }
        
        sendMessageTriger.subscribeNext { [weak self] message in
            guard let self = self else { return }
            self.sendMessageStreamingRelay.send((message, "")) { result in
                switch result {
                case .failure(_):
                    break
                case .success():
                    let messages = self.presenter.messages.value + [ChatMessage(type: .user, message:message)]
                    self.presenter.messages.accept(messages)
                }
            }
        }.disposeOnDeactivate(interactor: self)
    }
}
