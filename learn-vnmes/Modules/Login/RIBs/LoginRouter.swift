//
//  LoginRouter.swift
//  learn-vnmes
//
//  Created by Pham Anh Tuan on 11/24/19.
//  Copyright © 2019 Pham Anh Tuan. All rights reserved.
//

import RIBs
import PubNub

protocol LoginInteractable: Interactable, HomeListener {
    var router: LoginRouting? { get set }
    var listener: LoginListener? { get set }
}

protocol LoginViewControllable: ViewControllable {
    func present(viewController: ViewControllable)
    func dismiss(viewController: ViewControllable)
}

final class LoginRouter: ViewableRouter<LoginInteractable, LoginViewControllable>, LoginRouting {
    
    private let homeBuilder: HomeBuildable

    // TODO: Constructor inject child builder protocols to allow building children.
    init(interactor: LoginInteractable, viewController: LoginViewControllable, homeBuilder: HomeBuildable) {
        self.homeBuilder = homeBuilder
        super.init(interactor: interactor, viewController: viewController)
        interactor.router = self
    }
}

extension LoginRouter{
    func detachHome() {
        if let routing = children.first(where: { $0 is HomeRouting }) as? HomeRouting {
            viewController.dismiss(viewController: routing.viewControllable)
             detachChild(routing)
        }
    }
    
    func routeToHome() {
        let routing = homeBuilder.build(withListener: interactor)
              attachChild(routing)
              viewController.present(viewController: routing.viewControllable)
    }
}

