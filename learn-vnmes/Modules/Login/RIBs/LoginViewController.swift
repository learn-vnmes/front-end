//
//  LoginViewController.swift
//  learn-vnmes
//
//  Created by Pham Anh Tuan on 11/24/19.
//  Copyright © 2019 Pham Anh Tuan. All rights reserved.
//

import RIBs
import RxSwift
import UIKit
import RxCocoa
import Kingfisher

protocol LoginPresentableListener: class {
    var loginSuccessedRelay: PublishRelay<(username: String, password: String)> { get }
}

final class LoginViewController: UIViewController, LoginPresentable, LoginViewControllable {

    func present(viewController: ViewControllable) {
         viewController.uiviewController.modalPresentationStyle = .fullScreen
        configureStartView()
         present(viewController.uiviewController, animated: true, completion: nil)
     }
     
     func dismiss(viewController: ViewControllable) {
           if presentedViewController === viewController.uiviewController {
               dismiss(animated: true, completion: nil)
           }
       }
    

    weak var listener: LoginPresentableListener?
    private let disposeBag = DisposeBag()
    
    var gameTitleRelay = BehaviorRelay<String>(value: "1234")
    var loginBannerRelay = BehaviorRelay<String>(value: "")
    var loginError: BehaviorRelay<String> = BehaviorRelay(value: "")
    
    var viewDidloadRelay = BehaviorRelay<Bool>(value: false)
    
    @IBOutlet weak var backgroundImageView: UIImageView!
    @IBOutlet weak var heading: UILabel!
    @IBOutlet weak var username: UITextField!
    @IBOutlet weak var password: UITextField!
    @IBOutlet weak var logginButton: UIButton!
    @IBOutlet weak var titleLabel: UILabel!
    
    @IBOutlet weak var cloud1: UIImageView!
    @IBOutlet weak var cloud2: UIImageView!
    @IBOutlet weak var cloud3: UIImageView!
    @IBOutlet weak var clould4: UIImageView!
    
    @IBOutlet weak var loginButtonTopConstraint: NSLayoutConstraint!
    let status = UIImageView(image: Asset.Login.banner.image)
    var statusPosition: CGPoint = .zero
    let label = UILabel()
    let messages = ["Connecting ...", "Authorizing ...", "Sending credentials ...", "Failed"]
    
    let spinner: UIActivityIndicatorView = UIActivityIndicatorView(style: UIActivityIndicatorView.Style.whiteLarge)
    private let critterView: CritterView = CritterView(frame: CGRect(x: 0, y: 0, width: 160, height: 160))
    @IBOutlet weak var mainCritterView: UIView!
    
    
    private lazy var showHidePasswordButton = self.createShowHideButton()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        defer {
            viewDidloadRelay.accept(true)
        }
        configureView()
        configurePresenter()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        configureStartView()
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    private func configureStartView() {
        let formGroup = CAAnimationGroup()
        formGroup.duration = 0.5
        formGroup.fillMode = .backwards
        
        let flyRight = CABasicAnimation(keyPath: "position.x")
        flyRight.fromValue = -view.frame.width/2
        flyRight.toValue = view.frame.width/2
        
        let fadeFieldIn = CABasicAnimation(keyPath: "opacity")
        fadeFieldIn.fromValue = 0.25
        fadeFieldIn.toValue = 1.0
    
        formGroup.animations = [flyRight, fadeFieldIn]
        formGroup.delegate = self
        formGroup.setValue("form", forKey: "name")
        
       zip([username.layer, password.layer], [0.3, 0.4]).forEach { (layer, intervalTime) in
            formGroup.setValue(layer, forKey: "layer")
            formGroup.beginTime = CACurrentMediaTime() + intervalTime
            layer.add(formGroup, forKey: nil)
        }
        
        [cloud1, cloud2, cloud3, clould4].forEach { cloud in
            guard let cloud = cloud else { return }
            cloud.alpha = 0.0
        }
        
    }
    
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        username.delegate = self
        password.delegate = self
        
        let clouds = [cloud1, cloud2, cloud3, clould4]
        
        zip(clouds, [0.5,0.7,0.9,1.1]).forEach { (cloud, delay) in
            UIView.animate(withDuration: 0.5, delay: delay, options: [], animations: {
                cloud?.alpha = 1.0
            }, completion: nil)
        }
        
        clouds.forEach { cloud in
            guard let cloud = cloud else { return }
            animatedCloud(cloud)
        }
        
        
    }
    
    private func animatedCloud(_ cloud: UIImageView) {
        let cloudSpeed = 30 / view.frame.size.width
        let duration = (view.frame.size.width - cloud.frame.origin.x) * cloudSpeed
        
        UIView.animate(withDuration: TimeInterval(duration), delay: 0.0, options: .curveLinear, animations: {
             cloud.frame.origin.x = self.view.frame.size.width
        }) { _ in
            cloud.frame.origin.x = -cloud.frame.size.width
                  self.animatedCloud(cloud)
        }
        
    }
    
    private func configureView() {
        configurePasswordTextfield()
        configureCritterView()
        configureContent()
        configureBackground()
        configureLoginButton()
        configureBannerView()
        configureStatusLoading()
    }
    
    private func createShowHideButton() -> UIButton {
        let button = UIButton(type: .custom)
        button.setImage(#imageLiteral(resourceName: "Password-show"), for: .normal)
        button.setImage(#imageLiteral(resourceName: "Password-hide"), for: .selected)
        
        button.imageEdgeInsets = UIEdgeInsets(top: 0, left: -16, bottom: 0, right: 0)
        
        button.frame = CGRect(x: CGFloat(password.frame.size.width - 25), y: CGFloat(5), width: CGFloat(25), height: CGFloat(25))
        button.addTarget(self, action: #selector(togglePasswordVisibility(_:)), for: .touchUpInside)
        return button
    }
    
    private func configurePasswordTextfield() {
       
        password.rightView = showHidePasswordButton
        password.rightViewMode = .always
        
    }
    
    @objc private func togglePasswordVisibility(_ sender: UIButton) {
          sender.isSelected.toggle()
          let isPasswordVisible = sender.isSelected
          password.isSecureTextEntry = !isPasswordVisible
          critterView.isPeeking = isPasswordVisible

          // 🎩✨ Magic to fix cursor position when toggling password visibility
          if let textRange = password.textRange(from: password.beginningOfDocument, to: password.endOfDocument), let password = password.text {
            self.password.replace(textRange, withText: password)
          }
      }
    
    private func configureCritterView() {
        
        view.addSubview(critterView)
        critterView.translatesAutoresizingMaskIntoConstraints = false
        critterView.heightAnchor.constraint(equalToConstant: 160).isActive = true
        critterView.widthAnchor.constraint(equalToConstant: 160).isActive = true
        critterView.topAnchor.constraint(equalTo: mainCritterView.topAnchor).isActive = true
        critterView.leftAnchor.constraint(equalTo: mainCritterView.leftAnchor).isActive = true
    }
    
    private func configureStatusLoading() {
        status.isHidden = true
        status.center = logginButton.center
        view.addSubview(status)
        
        label.frame = CGRect(x: 0.0, y: 0.0, width: status.frame.size.width, height: status.frame.size.height)
        label.font = UIFont(name: "HelveticaNeue", size: 18.0)
        label.textColor = UIColor(red: 0.89, green: 0.38, blue: 0.0, alpha: 1.0)
        label.textAlignment = .center
        status.addSubview(label)
        
        statusPosition = status.center
    }
    
    private func configureContent() {
        logginButton.setTitle(L10n.loginButton, for: .normal)
    }
    
    private func configureBackground(){
        backgroundImageView.image = Asset.Login.loginIcPlaceholderBackgroud.image
        
    }
    
    private func configureLoginButton() {
        spinner.frame = CGRect(x: -20 , y: 6, width: 20, height: 20)
        spinner.startAnimating()
        spinner.alpha = 0
        logginButton.addSubview(spinner)
        
        logginButton.layer.cornerRadius = 8
        logginButton.layer.masksToBounds = true
        
        
        logginButton.rx.tap.asDriver(onErrorJustReturn: ())
            .drive(onNext: { [weak self] in
                guard let self = self else { return }
                self.didTapLoginButton()
            })
            .disposed(by: disposeBag)
        
        Observable.combineLatest(username.rx.text.asObservable(), password.rx.text.asObservable())
            .map { [weak self] username, password-> Bool in
                guard let self = self, let name = username, name.isNotEmpty, let pass = password, pass.isNotEmpty else { return false }
                if name.count < 6 { return false }
                if pass.count < 6 { return false }
                return true
            }
        .subscribeNext({ [weak self] isEnabled in
            guard let self = self else { return }
            self.logginButton.isEnabled = isEnabled
            if isEnabled {
                self.logginButton.backgroundColor = .red
            } else {
                self.logginButton.backgroundColor = .gray
            }
        })
            .disposed(by: disposeBag)
        
    }
    
    @IBAction func userNameDidChange(_ textField: UITextField) {
        guard !critterView.isActiveStartAnimating, textField == username else { return }

        let fractionComplete = self.fractionComplete(for: textField)
        critterView.updateHeadRotation(to: fractionComplete)

        if let text = textField.text {
            critterView.isEcstatic = text.contains("@")
        }
    }
    
    @IBAction func passWordDidChange(_ textField: UITextField) {
        guard !critterView.isActiveStartAnimating, textField == username else { return }

               let fractionComplete = self.fractionComplete(for: textField)
               critterView.updateHeadRotation(to: fractionComplete)

               if let text = textField.text {
                   critterView.isEcstatic = text.contains("@")
               }
    }
    
    
    
    private func configureBannerView(){
        loginBannerRelay.asDriver(onErrorJustReturn: "")
            .drive(onNext: { [weak self] bannerUrl in
            guard let self = self, let url = URL(string: bannerUrl) else { return }
            self.backgroundImageView.kf.setImage(with: url)
            }).disposed(by: disposeBag)
    }
}

extension LoginViewController {
    
    func delay(_ seconds: Double, completion: @escaping ()->Void) {
      DispatchQueue.main.asyncAfter(deadline: .now() + seconds, execute: completion)
    }
    
    private func didTapLoginButton() {
//        UIView.animate(withDuration: 1.5, delay: 0.0, usingSpringWithDamping: 0.2, initialSpringVelocity: 9.2, options: [], animations: {
//            self.logginButton.bounds.size.width += 100
//        }, completion: { _ in
//            self.showMessgae(messageIndex: 0)
//        })
//        
//        UIView.animate(withDuration: 0.5, delay: 0.0, usingSpringWithDamping: 0.7, initialSpringVelocity: 0.0, options: [], animations: {
//            self.loginButtonTopConstraint.constant += 60
//            self.spinner.center = CGPoint(x: 20, y: self.logginButton.frame.height/2)
//            self.spinner.alpha = 1.0
//        }, completion: nil)

        let usernameText = username.text ?? ""
        let passwordText = password.text ?? ""
        
        self.listener?.loginSuccessedRelay.accept((username: usernameText, password: passwordText))
    }
     
    private func showMessgae(messageIndex: Int) {
        label.text = messages[messageIndex]
        UIView.transition(with: status, duration: 0.33, options: [.curveEaseOut, .transitionCurlDown], animations: {
            self.status.isHidden = false
        }) { _ in
            self.delay(1.0) {
                if messageIndex < self.messages.count - 1 {
                    self.removeMessage(messgageIndex:  messageIndex)
                } else {
                    
                }
            }
        }
    }
    
    private func removeMessage(messgageIndex: Int) {
        UIView.animate(withDuration: 0.33, delay: 09.0, options: [], animations: {
            self.status.center.x += self.view.frame.size.width
        }) { _ in
            self.status.isHidden = true
            self.status.center = self.statusPosition
            self.showMessgae(messageIndex: messgageIndex + 1)
        }
    }
}

extension LoginViewController: CAAnimationDelegate {
    func animationDidStop(_ anim: CAAnimation, finished flag: Bool) {
        guard let name = anim.value(forKey: "name") as? String else { return }
        if name == "form" {
            guard let layer = anim.value(forKey: "layer") as? CALayer else { return }
            anim.setValue(nil, forKey: "layer")
            let pulse = CASpringAnimation(keyPath: "transform.scale")
            pulse.damping = 7.5
            pulse.fromValue = 1.25
            pulse.toValue = 1.0
            pulse.duration = pulse.settlingDuration
            layer.add(pulse, forKey: nil)
        }
        
        if name == "cloud" {
            ///write some code in here for implenment keyframe
        }
    }
    
    func animationDidStart(_ anim: CAAnimation) {
        
    }
}


extension LoginViewController: UITextFieldDelegate {
    func textFieldDidBeginEditing(_ textField: UITextField) {
        let deadlineTime = DispatchTime.now() + .milliseconds(100)
        
        if textField == username {
            DispatchQueue.main.asyncAfter(deadline: deadlineTime) { // 🎩✨ Magic to ensure animation starts
                let fractionComplete = self.fractionComplete(for: textField)
                self.critterView.startHeadRotation(startAt: fractionComplete)
                self.passwordDidResignAsFirstResponder()
            }
        }  else if textField == password {
            DispatchQueue.main.asyncAfter(deadline: deadlineTime) { // 🎩✨ Magic to ensure animation starts
                self.critterView.isShy = true
                self.showHidePasswordButton.isHidden = false
            }
        }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField == username {
            password.becomeFirstResponder()
        }
        else {
            password.resignFirstResponder()
            passwordDidResignAsFirstResponder()
        }
        return true
    }

    func textFieldDidEndEditing(_ textField: UITextField) {
        if textField == username {
            critterView.stopHeadRotation()
        }
    }
    private func passwordDidResignAsFirstResponder() {
          critterView.isPeeking = false
          critterView.isShy = false
          showHidePasswordButton.isHidden = true
          showHidePasswordButton.isSelected = false
          password.isSecureTextEntry = true
      }
    
    private func fractionComplete(for textField: UITextField) -> Float {
        guard let text = textField.text, let font = textField.font else { return 0 }
        let textFieldWidth = textField.bounds.width - (2 * HomeConfig.textFieldHorizontalMargin)
        return min(Float(text.size(withAttributes: [NSAttributedString.Key.font : font]).width / textFieldWidth), 1)
    }
    
}

extension LoginViewController {
    private func configurePresenter() {
        loginError
            .asDriver(onErrorJustReturn: "")
            .filterEmpty()
            .driveNext({ [weak self] errDescription in
                guard let self = self else { return }
                self.showLoginErrorView(errorDescription: errDescription)
                
            })
            .disposed(by: disposeBag)
    }
}

extension LoginViewController {
    enum HomeConfig {
        static let textFieldHorizontalMargin:CGFloat = 40.0
    }
}

extension LoginViewController {
    private func showLoginErrorView(errorDescription err: String) {
        let alert = UIAlertController(title: "Ôi toang rồi", message: err, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Té thôi", style: .cancel, handler: nil))
        self.present(alert, animated: true)
    }
}
