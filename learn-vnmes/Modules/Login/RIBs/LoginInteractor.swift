//
//  LoginInteractor.swift
//  learn-vnmes
//
//  Created by Pham Anh Tuan on 11/24/19.
//  Copyright © 2019 Pham Anh Tuan. All rights reserved.
//

import RIBs
import RxSwift
import RxCocoa
import SwiftGRPCClient

protocol LoginRouting: ViewableRouting {
    func routeToHome()
    func detachHome()
}

protocol LoginPresentable: Presentable {
    var listener: LoginPresentableListener? { get set }
    var gameTitleRelay: BehaviorRelay<String> { get }
    var loginBannerRelay: BehaviorRelay<String> { get }
    var loginError: BehaviorRelay< String> { get set}
}

protocol LoginListener: class {
    func detachLogin()
}

final class LoginInteractor: PresentableInteractor<LoginPresentable>, LoginInteractable, LoginPresentableListener {
    

    weak var router: LoginRouting?
    weak var listener: LoginListener?
    
    let remoteConfig: LoginRemoteConfigManagerType

    // MARK: - Listener
    var loginSuccessedRelay: PublishRelay<(username: String, password: String)> = PublishRelay()
    var loginServerObservable: SwiftGRPCClient.Stream<LoginRequest>?
    
    init(presenter: LoginPresentable, remoteConfig: LoginRemoteConfigManagerType) {
        self.remoteConfig = remoteConfig
        super.init(presenter: presenter)
        presenter.listener = self
    }
    
    private func fetchRemoteConfigData() {
        updateLoginGameTitle()
        updateLoginBanner()
    }
    
    private func updateLoginGameTitle() {
        let gameTitle = remoteConfig.title
        print("OK \(gameTitle)")
        presenter.gameTitleRelay.accept(gameTitle)
    }
    
    private func updateLoginBanner() {
        let loginBannerUrl = remoteConfig.bannerUrl
        presenter.loginBannerRelay.accept(loginBannerUrl)
    }

    override func didBecomeActive() {
        super.didBecomeActive()
        fetchRemoteConfigData()
        configureListener()
    }
    
    

    override func willResignActive() {
        super.willResignActive()
        // TODO: Pause any business logic.
    }
}

private extension LoginInteractor{
    private func configureListener(){
        
        loginSuccessedRelay
            .subscribe(onNext: { [weak self] loginInfo in
                guard let self = self else { return }
                self.login(username: loginInfo.username, password: loginInfo.password)
                
            })
            .disposeOnDeactivate(interactor: self)
    }
    
    func login(username: String, password: String) {
        loginServerObservable = ProfileServiceSesion.shared.stream(with: LoginRequest(username: username, password:password)).data({ [weak self] data in
            guard let self = self else { return }
//            UserDefaults.standard.authenticationToken = data.
            
            switch data {
            case let .failure(err):
                print(err)
                DispatchQueue.main.async {
                    self.presenter.loginError.accept(err.localizedDescription)
                }
                
            case let .success(result):
                UserDefaults.standard.authenticationToken = result.token
                DispatchQueue.main.async {
                    self.router?.routeToHome()
                }
            }
            
            print(data)
               
            })
    }
    
    func register() {
        self.router?.routeToHome()
    }
    
}

extension LoginInteractor {
    func detachHome() {
        self.router?.detachHome()
    }
}


