//
//  LoginBuilder.swift
//  learn-vnmes
//
//  Created by Pham Anh Tuan on 11/24/19.
//  Copyright © 2019 Pham Anh Tuan. All rights reserved.
//

import RIBs

protocol LoginDependency: Dependency {
    // TODO: Declare the set of dependencies required by this RIB, but cannot be
    // created by this RIB.
}

final class LoginComponent: Component<LoginDependency> {

    // TODO: Declare 'fileprivate' dependencies that are only used by this RIB.
}

extension LoginComponent: HomeDependency{}

// MARK: - Builder

protocol LoginBuildable: Buildable {
    func build(withListener listener: LoginListener) -> LoginRouting
}

final class LoginBuilder: Builder<LoginDependency>, LoginBuildable {

    override init(dependency: LoginDependency) {
        super.init(dependency: dependency)
    }

    func build(withListener listener: LoginListener) -> LoginRouting {
        let component = LoginComponent(dependency: dependency)
        let viewController = StoryboardScene.LoginViewController.initialScene.instantiate()
        let remoteConfig = LoginRemoteConfigManager.default
        let interactor = LoginInteractor(presenter: viewController, remoteConfig: remoteConfig)
        interactor.listener = listener
        let homeBuilder = HomeBuilder(dependency: component)
        return LoginRouter(interactor: interactor, viewController: viewController, homeBuilder: homeBuilder)
    }
}
