//
//  DataRepository.swift
//  learn-vnmes
//
//  Created by Pham Anh Tuan on 1/6/20.
//  Copyright © 2020 Pham Anh Tuan. All rights reserved.
//

import Foundation
import SwiftGRPC
import RxSwift
import RxCocoa


class ListFeatureRequestBuilder {
    var requestObject: V1_Rectangle = V1_Rectangle()
    private var hiPosition: V1_Point = V1_Point()
    private var loPosition: V1_Point = V1_Point()
    
    
    func setHeightPosition(longitude: Int32 = .zero,latitude: Int32 = .zero) -> ListFeatureRequestBuilder {
            hiPosition = V1_Point.with {
                $0.longitude = longitude
                $0.latitude = latitude
            }
            return self
    }
    
    func setLowerPosition(longitude: Int32 = .zero, latitude: Int32 = .zero) -> ListFeatureRequestBuilder {
             loPosition = V1_Point.with {
                 $0.longitude = longitude
                 $0.latitude = latitude
             }
            
            return self
     }
    
    func build() -> V1_Rectangle {
        return V1_Rectangle.with {
            $0.hi = hiPosition
            $0.lo = loPosition
        }
    }
}



//protocol ProfileServiceType{
//    func register(request: RegisterRequest) -> Observable<Account>
//}

//class ProfileService: ProfileServiceType {
//
//
//    func register(request: RegisterRequest) -> Observable<Account> {
//        return GRPCSerivce.request(DataRepository.shar)
//    }
//}
//
//class GRPCSerivce {
//    static func request(_ fn: @escaping( @escaping (RegisterResponse?, CallResult) -> Void) throws -> ProfileServiceRegisterCall) -> Observable<RegisterResponse> {
//        return Observable.create { observable in
//            try? fn { response, result in
//                if let dataResponse = response {
//                    observable.onNext(dataResponse)
//                } else {
//                    observable.onError(NSError(domain: result.description, code: result.statusCode.rawValue, userInfo: [:]))
//                }
//            }
//
//            return Disposables.create()
//        }
//    }
//}
