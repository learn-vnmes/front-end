//
//  LoginRemoteConfig.swift
//  learn-vnmes
//
//  Created by Pham Anh Tuan on 11/25/19.
//  Copyright © 2019 Pham Anh Tuan. All rights reserved.
//

import Foundation
public protocol LoginRemoteConfigManagerType: AnyObject {
    var title: String { get }
    var bannerUrl: String { get }
}

public final class LoginRemoteConfigManager: LoginRemoteConfigManagerType {
    public var title: String {
        remoteConfig.string(forKey: Key.loginTitle, defaultValue: Default.loginTitle, replaceEmpty: true)
    }
    
    public var bannerUrl: String {
        remoteConfig.string(forKey: Key.lgoinBannerUrl, defaultValue: Default.lgoinBannerUrl, replaceEmpty: true)
    }
    
    public static let `default`: LoginRemoteConfigManagerType = LoginRemoteConfigManager()
    private let remoteConfig: AppRemoteConfigManagerType
    
    private init(remoteConfig: AppRemoteConfigManagerType = AppRemoteConfigManager.default) {
        self.remoteConfig = remoteConfig
    }
    
    private enum Key {
        static let loginTitle: String = "login_title"
        static let lgoinBannerUrl: String = "login_banner_url"
    }
    
    private enum Default {
        static let loginTitle = "unknowned"
        static let lgoinBannerUrl = ""
    }
}
