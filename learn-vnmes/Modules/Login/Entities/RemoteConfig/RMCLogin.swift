//
//  RMCLogin.swift
//  learn-vnmes
//
//  Created by Pham Anh Tuan on 11/25/19.
//  Copyright © 2019 Pham Anh Tuan. All rights reserved.
//

import Foundation
import ObjectMapper


public struct RMCLogin: Codable {
    
    public static let `default` = RMCLogin()
    public let title: String?
    public let bannerUrl: String?
    
    init() {
        self.title = ""
        self.bannerUrl = ""
    }

    enum CodingKeys: String, CodingKey {
        case title
        case bannerUrl = "banner_url"
    }
}
