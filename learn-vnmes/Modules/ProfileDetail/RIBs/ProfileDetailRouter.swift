//
//  ProfileDetailRouter.swift
//  learn-vnmes
//
//  Created by Pham Anh Tuan on 2/22/20.
//  Copyright © 2020 Pham Anh Tuan. All rights reserved.
//

import RIBs

protocol ProfileDetailInteractable: Interactable {
    var router: ProfileDetailRouting? { get set }
    var listener: ProfileDetailListener? { get set }
}

protocol ProfileDetailViewControllable: ViewControllable {
    // TODO: Declare methods the router invokes to manipulate the view hierarchy.
}

final class ProfileDetailRouter: ViewableRouter<ProfileDetailInteractable, ProfileDetailViewControllable>, ProfileDetailRouting {

    // TODO: Constructor inject child builder protocols to allow building children.
    override init(interactor: ProfileDetailInteractable, viewController: ProfileDetailViewControllable) {
        super.init(interactor: interactor, viewController: viewController)
        interactor.router = self
    }
}
