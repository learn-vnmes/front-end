//
//  ProfileDetailBuilder.swift
//  learn-vnmes
//
//  Created by Pham Anh Tuan on 2/22/20.
//  Copyright © 2020 Pham Anh Tuan. All rights reserved.
//

import RIBs

protocol ProfileDetailDependency: Dependency {
    // TODO: Declare the set of dependencies required by this RIB, but cannot be
    // created by this RIB.
}

final class ProfileDetailComponent: Component<ProfileDetailDependency> {

    // TODO: Declare 'fileprivate' dependencies that are only used by this RIB.
}

// MARK: - Builder

protocol ProfileDetailBuildable: Buildable {
    func build(withListener listener: ProfileDetailListener) -> ProfileDetailRouting
}

final class ProfileDetailBuilder: Builder<ProfileDetailDependency>, ProfileDetailBuildable {

    override init(dependency: ProfileDetailDependency) {
        super.init(dependency: dependency)
    }

    func build(withListener listener: ProfileDetailListener) -> ProfileDetailRouting {
        let component = ProfileDetailComponent(dependency: dependency)
        let viewController = ProfileDetailViewController()
        let interactor = ProfileDetailInteractor(presenter: viewController)
        interactor.listener = listener
        return ProfileDetailRouter(interactor: interactor, viewController: viewController)
    }
}
