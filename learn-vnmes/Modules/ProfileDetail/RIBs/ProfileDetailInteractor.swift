//
//  ProfileDetailInteractor.swift
//  learn-vnmes
//
//  Created by Pham Anh Tuan on 2/22/20.
//  Copyright © 2020 Pham Anh Tuan. All rights reserved.
//

import RIBs
import RxSwift

protocol ProfileDetailRouting: ViewableRouting {
    // TODO: Declare methods the interactor can invoke to manage sub-tree via the router.
}

protocol ProfileDetailPresentable: Presentable {
    var listener: ProfileDetailPresentableListener? { get set }
    // TODO: Declare methods the interactor can invoke the presenter to present data.
}

protocol ProfileDetailListener: class {
    // TODO: Declare methods the interactor can invoke to communicate with other RIBs.
}

final class ProfileDetailInteractor: PresentableInteractor<ProfileDetailPresentable>, ProfileDetailInteractable, ProfileDetailPresentableListener {

    weak var router: ProfileDetailRouting?
    weak var listener: ProfileDetailListener?

    // TODO: Add additional dependencies to constructor. Do not perform any logic
    // in constructor.
    override init(presenter: ProfileDetailPresentable) {
        super.init(presenter: presenter)
        presenter.listener = self
    }

    override func didBecomeActive() {
        super.didBecomeActive()
        // TODO: Implement business logic here.
    }

    override func willResignActive() {
        super.willResignActive()
        // TODO: Pause any business logic.
    }
}
