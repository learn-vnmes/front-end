//
//  ProfileDetailViewController.swift
//  learn-vnmes
//
//  Created by Pham Anh Tuan on 2/22/20.
//  Copyright © 2020 Pham Anh Tuan. All rights reserved.
//

import RIBs
import RxSwift
import UIKit

protocol ProfileDetailPresentableListener: class {
    // TODO: Declare properties and methods that the view controller can invoke to perform
    // business logic, such as signIn(). This protocol is implemented by the corresponding
    // interactor class.
}

final class ProfileDetailViewController: UIViewController, ProfileDetailPresentable, ProfileDetailViewControllable {

    weak var listener: ProfileDetailPresentableListener?
}
