//
//  HomeViewController.swift
//  learn-vnmes
//
//  Created by Pham Anh Tuan on 11/27/19.
//  Copyright © 2019 Pham Anh Tuan. All rights reserved.
//

import RIBs
import RxSwift
import UIKit
import RxCocoa
import RxDataSources
import SwiftGRPCClient
import Reusable

protocol HomePresentableListener: class {
    // TODO: Declare properties and methods that the view controller can invoke to perform
    // business logic, such as signIn(). This protocol is implemented by the corresponding
    // interactor class.
    var viewDidloadRelay: BehaviorRelay<Bool> { get set}
    var selectedChatToServer: PublishRelay<Bool> { get set }
    var logoutRelay: PublishRelay<Void> { get }
    
}

final class HomeViewController: UIViewController, HomePresentable, HomeViewControllable , NibOwnerLoadable{
    
    typealias Section = SectionModel<String, FilmCategoryViewModel>
    typealias DataSource = RxCollectionViewSectionedReloadDataSource<Section>
    lazy var dataSource = makeDataSources()

    weak var listener: HomePresentableListener?
    
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var logoutButton: UIButton!
    @IBOutlet weak var wrapAvartarView: UIView!
    @IBOutlet weak var avartarImageView: UIImageView!
    @IBOutlet weak var chatToServerButton: UIButton!
    var data: BehaviorRelay<[V1_FilmCategory]> = BehaviorRelay(value: [])
    private let disposeBag = DisposeBag()
    
    
    
    
    var viewDidloadRelay = BehaviorRelay<Bool>(value: false)
    
    func makeDataSources() -> DataSource {
        let configureCell: DataSource.ConfigureCell = { (_, collectionView, indexPath, viewModel) -> UICollectionViewCell in
            let cell: CategoryFashionCell = collectionView.dequeueReusableCell(for: indexPath)
            cell.configureCell(viewModel: viewModel)
            return cell
        }
        
        let configureSupplementaryView: DataSource.ConfigureSupplementaryView = {(_, collectionView, kind, indexPath) -> UICollectionReusableView in
            let header: HeaderView = collectionView.dequeueReusableSupplementaryView(ofKind: kind, for: indexPath)
            return header
        }
        
        return DataSource(configureCell: configureCell, configureSupplementaryView: configureSupplementaryView)
    }
    
    func present(viewController: ViewControllable) {
         viewController.uiviewController.modalPresentationStyle = .fullScreen
         present(viewController.uiviewController, animated: true, completion: nil)
     }
     
     func dismiss(viewController: ViewControllable) {
           if presentedViewController === viewController.uiviewController {
               dismiss(animated: true, completion: nil)
           }
       }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        defer {
            listener?.viewDidloadRelay.accept(true)
        }
        configureViews()
        configureListeners()
       
    }
    
    
    override var prefersStatusBarHidden: Bool {
        return true
    }
    
    func configureViews() {
        configureCollectionView()
        configureWrapAvartarView()
        configureChatToServerButton()
    }
    
    func configureWrapAvartarView() {
        wrapAvartarView.layer.cornerRadius = 30.0
        avartarImageView.layer.cornerRadius = 30.0
        wrapAvartarView.drawBorder(color: #colorLiteral(red: 0.2605174184, green: 0.2605243921, blue: 0.260520637, alpha: 1), width: 1.0)
    }
    
    func configureCollectionView() {
        collectionView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 16, right: 0)
         if let layout = collectionView.collectionViewLayout as? UICollectionViewFlowLayout {
             layout.itemSize = CGSize(width: UIScreen.main.bounds.width - 2 * 16, height: 160)
            layout.headerReferenceSize = CGSize(width: UIScreen.main.bounds.width, height: 82)
         }
        
        collectionView.register(UICollectionViewCell.self, forCellWithReuseIdentifier: "cell")
        collectionView.register(cellType: CategoryFashionCell.self)
        collectionView.register(supplementaryViewType: HeaderView.self, ofKind: UICollectionView.elementKindSectionHeader)
        

        data.asDriver(onErrorJustReturn: [])
            .map({ categories -> [Section] in
                let items = categories.map { model  in
                    return FilmCategoryViewModel(model: model)
                }
                return [Section(model: "feature_section", items: items)]
            })
            .drive(collectionView.rx.items(dataSource: dataSource))
            .disposed(by: disposeBag)
    }
    
    private func configureChatToServerButton() {
        chatToServerButton.rx.tap.subscribeNext { [weak self] in
            guard let self = self else { return }
            self.listener?.selectedChatToServer.accept(true)
            
        }
        .disposed(by: disposeBag)
    }
}

extension HomeViewController {
    private func configureListeners() {
        logoutButton.rx.tap.subscribeNext { [weak self]  in
            guard let self = self else { return }
            self.listener?.logoutRelay.accept(())
        }
        .disposed(by: disposeBag)
    }
}
