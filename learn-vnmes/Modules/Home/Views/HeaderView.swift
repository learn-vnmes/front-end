//
//  HeaderView.swift
//  learn-vnmes
//
//  Created by Pham Anh Tuan on 2/20/20.
//  Copyright © 2020 Pham Anh Tuan. All rights reserved.
//

import UIKit
import Reusable

class HeaderView: UICollectionReusableView, NibReusable {

    @IBOutlet weak var wrapSearchView: UIView!
    override func awakeFromNib() {
        super.awakeFromNib()
        configureViews()
    }
    
    private func configureViews() {
        wrapSearchView.layer.cornerRadius = 12.0 
        wrapSearchView.dropShadow(color: .black, offset: CGSize(width: 0.5, height: 0.5), opacity: 0.5, radius: 4)
    }
    
}
