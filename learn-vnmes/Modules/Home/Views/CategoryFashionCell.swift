//
//  CategoryFashionCell.swift
//  learn-vnmes
//
//  Created by Pham Anh Tuan on 2/20/20.
//  Copyright © 2020 Pham Anh Tuan. All rights reserved.
//

import UIKit
import Reusable
import Kingfisher

struct FilmCategoryViewModel {
    
    let backgroundUrl: URL?
    let title: String
    
    init(model: V1_FilmCategory) {
        backgroundUrl = URL(string: model.imageURL)
        title = model.name
    }
    
}
class CategoryFashionCell: UICollectionViewCell, NibReusable {
    
    
    @IBOutlet weak var backgroundImageView: UIImageView!
    @IBOutlet weak var wrapGradientView: UIView!
    @IBOutlet weak var mainView: UIView!
    @IBOutlet weak var titleLabel: UILabel!
    
    private lazy var gradientLayer: LinearGradientLayer = self.setupGradientLayer()
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        backgroundImageView.layer.cornerRadius = 10.0
        mainView.layer.cornerRadius = 10.0
        dropShadow(color: UIColor.black, offset: CGSize(width: 0, height: 8), opacity: 0.5, radius: 10)
        gradientLayer.frame = wrapGradientView.frame
        wrapGradientView.layer.addSublayer(gradientLayer)
    }
    
    func configureCell(viewModel: FilmCategoryViewModel ) {
        backgroundImageView.kf.setImage(with: viewModel.backgroundUrl)
        titleLabel.text = viewModel.title.uppercased()
    }
    
    private func setupGradientLayer() -> LinearGradientLayer {
        let gradientLayer = LinearGradientLayer()
        gradientLayer.colors = [UIColor(white: 0, alpha: 1.0).cgColor, UIColor(white: 0, alpha: 0.7).cgColor, UIColor.clear.cgColor]
        gradientLayer.locations = [1.0,0.5,0.0]
        return gradientLayer
    }

}
