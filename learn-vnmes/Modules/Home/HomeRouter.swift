//
//  HomeRouter.swift
//  learn-vnmes
//
//  Created by Pham Anh Tuan on 11/27/19.
//  Copyright © 2019 Pham Anh Tuan. All rights reserved.
//

import RIBs

protocol HomeInteractable: Interactable, ChatListener {
    var router: HomeRouting? { get set }
    var listener: HomeListener? { get set }
}

protocol HomeViewControllable: ViewControllable {
    func present(viewController: ViewControllable)
      func dismiss(viewController: ViewControllable)
  
}

final class HomeRouter: ViewableRouter<HomeInteractable, HomeViewControllable>, HomeRouting {

    
    private let chatBuilder: ChatBuilder
    
    init(interactor: HomeInteractable, viewController: HomeViewControllable, chatBuilder: ChatBuilder) {
        self.chatBuilder = chatBuilder
        super.init(interactor: interactor, viewController: viewController)
        interactor.router = self
    }
}

extension HomeRouter {
    func routeToChat() {
        let routing = chatBuilder.build(withListener: interactor)
        attachChild(routing)
        viewController.present(viewController: routing.viewControllable)
    }
    
    func detachChat() {
        if let routing = children.first(where: { $0 is ChatRouting }) as? ChatRouting {
            viewController.dismiss(viewController: routing.viewControllable)
            detachChild(routing)
        }
    }
}
