//
//  HomeInteractor.swift
//  learn-vnmes
//
//  Created by Pham Anh Tuan on 11/27/19.
//  Copyright © 2019 Pham Anh Tuan. All rights reserved.
//

import RIBs
import RxSwift
import RxCocoa
import SwiftGRPCClient

protocol HomeRouting: ViewableRouting {
    // TODO: Declare methods the interactor can invoke to manage sub-tree via the router.
    func routeToChat()
    func detachChat()
}

protocol HomePresentable: Presentable {
    var listener: HomePresentableListener? { get set }
    var data: BehaviorRelay<[V1_FilmCategory]> { get set }
    // TODO: Declare methods the interactor can invoke the presenter to present data.
}

protocol HomeListener: class {
    // TODO: Declare methods the interactor can invoke to communicate with other RIBs.
    func detachHome()
   
}

final class HomeInteractor: PresentableInteractor<HomePresentable>, HomeInteractable, HomePresentableListener {

    weak var router: HomeRouting?
    weak var listener: HomeListener?
    
    let routeChatService: RouteChatServiceType
    var fetchCategoriesRelay = FilmService.shared.stream(with: ListFilmCategoriesRequest())
    
    var selectedChatToServer: PublishRelay<Bool> = PublishRelay<Bool>()
    var viewDidloadRelay: BehaviorRelay<Bool> = BehaviorRelay(value: false)
    var logoutRelay: PublishRelay<Void> = PublishRelay<Void> ()

    init(presenter: HomePresentable, routeChatService: RouteChatServiceType) {
        self.routeChatService = routeChatService

        super.init(presenter: presenter)
        presenter.listener = self
    }

    override func didBecomeActive() {
        super.didBecomeActive()
        fetchData()
        configureListener()
    }

    override func willResignActive() {
        super.willResignActive()
        // TODO: Pause any business logic.
    }
    
}

extension HomeInteractor {
    
    func fetchData() {
        fetchCategories()
    }
    
    func fetchCategories() {
        
       fetchCategoriesRelay
            .create()
            .subscribeNext {[weak self] category in
                guard let self = self else { return }
                
                var categories = self.presenter.data.value
                categories.append(category)
                
                self.presenter.data.accept(categories)
            }
            .disposeOnDeactivate(interactor: self)
        
    }
}

extension HomeInteractor {
    private func configureListener() {
        
        
        selectedChatToServer
            .filter { $0 }
            .subscribeNext { [weak self] _ in
                guard let self = self else { return }
                self.router?.routeToChat()
            }
        .disposeOnDeactivate(interactor: self)
        
        logoutRelay
            .subscribeNext { [weak self] _ in
                guard let self = self else { return }
                self.listener?.detachHome()
            }
            .disposeOnDeactivate(interactor: self)
    }
}

extension HomeInteractor {
    func detachChat() {
        self.router?.detachChat()
    }
}
