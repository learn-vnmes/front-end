//
//  HomeBuilder.swift
//  learn-vnmes
//
//  Created by Pham Anh Tuan on 11/27/19.
//  Copyright © 2019 Pham Anh Tuan. All rights reserved.
//

import RIBs

protocol HomeDependency: Dependency {
    // TODO: Declare the set of dependencies required by this RIB, but cannot be
    // created by this RIB.
}

final class HomeComponent: Component<HomeDependency> {

    // TODO: Declare 'fileprivate' dependencies that are only used by this RIB.
}
extension HomeComponent: ChatDependency {}

// MARK: - Builder

protocol HomeBuildable: Buildable {
    func build(withListener listener: HomeListener) -> HomeRouting
}

final class HomeBuilder: Builder<HomeDependency>, HomeBuildable {

    override init(dependency: HomeDependency) {
        super.init(dependency: dependency)
    }

    func build(withListener listener: HomeListener) -> HomeRouting {
        
        let routeChatService = Injectors.get(RouteChatServiceType.self, name: ServiceType.grpc.rawValue)
        let component = HomeComponent(dependency: dependency)
        let viewController = StoryboardScene.HomeViewController.initialScene.instantiate() 
        let interactor = HomeInteractor(presenter: viewController, routeChatService: routeChatService)
        interactor.listener = listener
        let chatBuilder = ChatBuilder(dependency: component)
        return HomeRouter(interactor: interactor, viewController: viewController, chatBuilder: chatBuilder)
    }
}
