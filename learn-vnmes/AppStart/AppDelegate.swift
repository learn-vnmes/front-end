//
//  AppDelegate.swift
//  learn-vnmes
//
//  Created by Pham Anh Tuan on 11/16/19.
//  Copyright © 2019 Pham Anh Tuan. All rights reserved.
//

import UIKit
import RIBs
import SwiftGRPC

@UIApplicationMain
public class AppDelegate: UIResponder, UIApplicationDelegate {

    public var window: UIWindow?

    let insecureServer = ServiceServer(address: "127.0.0.1", serviceProviders: [])
    public func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        
       
        Injectors.initialize(modules: DevModule())
        

        self.window = UIWindow(frame: UIScreen.main.bounds)
        guard let window = window else { return false}
        
        
        let launchRouter = RootBuilder(dependency: AppComponent()).build(with: window)
        self.launchRouter = launchRouter
        launchRouter.launchFromWindow(window)
        insecureServer.start()
        
        return true
    }
    
      private var launchRouter: LaunchRouting?
}

