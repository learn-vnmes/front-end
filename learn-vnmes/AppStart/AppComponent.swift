//
//  AppComponent.swift
//  learn-vnmes
//
//  Created by Pham Anh Tuan on 11/23/19.
//  Copyright © 2019 Pham Anh Tuan. All rights reserved.
//

import RIBs

class AppComponent: Component<EmptyDependency>, RootDependency {

    init() {
        super.init(dependency: EmptyComponent())
    }
}
