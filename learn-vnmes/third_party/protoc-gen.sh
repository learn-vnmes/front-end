#protoc --proto_path=api/proto/v1 --proto_path=third_party --go_out=plugins=grpc:pkg/api/v1 profile-service.proto
#protoc --proto_path=api/proto/v1 --proto_path=third_party --grpc-gateway_out=logtostderr=true:pkg/api/v1 profile-service.proto
#protoc --proto_path=api/proto/v1 --proto_path=third_party --swagger_out=logtostderr=true:api/swagger/v1 profile-service.proto

protoc Generated/proto/v1/profile-service.proto --proto_path=Generated/proto/v1  --proto_path=third_party  --plugin=grpc-swift-client/protoc-gen-swiftgrpc-client --swiftgrpc-client_out=Generated/.  --swift_out=Generated/.

protoc Generated/proto/v1/film-service.proto --proto_path=Generated/proto/v1  --proto_path=third_party  --plugin=grpc-swift-client/protoc-gen-swiftgrpc-client --swiftgrpc-client_out=Generated/.  --swift_out=Generated/.

#protoc --proto_path=Generated/proto/v1 --proto_path=third_party  --swiftgrpc_out=. Generated/proto/v1/profile-service.proto
